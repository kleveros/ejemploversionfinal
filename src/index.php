<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
</head>
<body>
	<div class="container">
	<?php require_once 'procesar.php'; ?>

	<?php if(isset($_SESSION['mensaje'])): ?>
	<div class="alert alert-<?php echo $_SESSION['tipo_msg'] ?> alert-dismissable">
		<?php 
			echo $_SESSION['mensaje'];
			unset($_SESSION['mensaje']); 
		?>
	</div>
	<?php endif; ?>



	<?php
	// $mysqli = new mysqli('localhost','root','','testprueba') or die(mysqli_error($mysqli));
	$result = $mysqli->query("SELECT * FROM test");
	// pre_r($result);
	// pre_r($result->fetch_assoc());


	// function pre_r($array){
	// 	echo "<pre>";
	// 	print_r($array);
	// 	echo "</pre>";
	// }
	?>

	<div class="row justify-content-center">
	<form action="procesar.php" method="post">
		<input type="hidden" name="id" value="<?php echo $id; ?>">
		<div class="form-group">
		<label>Usuario</label>
		<input type="text" name="usuario" class="form-control" placeholder="Ingrese un Usuario" value="<?php echo $usr;?>">
		</div>
		<div class="form-group">
		<label>Correo</label>
		<input type="text" name="correo" class="form-control" placeholder="Ingrese el correo" value="<?php echo $cor; ?>">
		</div>
		<div class="form-group">
		<label>Contraseña</label>
		<input type="test" name="contraseña" class="form-control" placeholder="Ingrese la clave" value="<?php echo $pwd; ?>">
		</div>
		<div class="form-group">
			<?php if($editar==TRUE): ?>
				<button type="submit" class="btn btn-warning" name="actualizar">Actualizar</button>
			<?php else: ?>
				<button type="submit" class="btn btn-primary" name="guardar">Guardar</button>
			<?php endif; ?>
		</div>
	</form>
	</div>
	
	<div class="row justify-content-center">
		<h1>Registros de Usuario</h1>
		<table class="table table-hover table-condensed">
			<thead>
				<th>Usuario</th>
				<th>Correo</th>
				<th>Clave</th>
				<th colspan="2">Opciones</th>
			</thead>
			<?php while($row = $result->fetch_assoc()): ?>
			<tr>
				<td><?php echo $row['usuario']; ?></td>
				<td><?php echo $row['correo']; ?></td>
				<td><?php echo $row['clave']; ?></td>
				<td>
					<a href="index.php?editar=<?php echo $row['id']; ?>" class="btn btn-info">Actualizar</a>
					<a href="procesar.php?eliminar=<?php echo $row['id']; ?>" class="btn btn-danger">Eliminar</a>
				</td>
			</tr>
			<?php endwhile; ?>
		</table>
		
	</div>
</div>
</body>
</html>