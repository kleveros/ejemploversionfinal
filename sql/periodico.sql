-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 29-01-2020 a las 16:29:58
-- Versión del servidor: 10.3.16-MariaDB
-- Versión de PHP: 7.1.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `periodico`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `noticias`
--

CREATE TABLE `noticias` (
  `id_noti` int(50) NOT NULL,
  `titulo_noti` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `texto_noti` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `categoria_noti` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `fecha_noti` date NOT NULL,
  `imagen_noti` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `nomim_noti` varchar(100) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `noticias`
--

INSERT INTO `noticias` (`id_noti`, `titulo_noti`, `texto_noti`, `categoria_noti`, `fecha_noti`, `imagen_noti`, `nomim_noti`) VALUES
(34, 'o', 'o', 'o', '2020-08-08', 'localhostconexion\recursossnorlax.jpg<br><br>', 'snorlax.jpg'),
(36, 'c', 'c', 'c', '2020-07-08', 'localhostconexion\recursoscharizard.jpg<br><br>', 'charizard.jpg'),
(37, 'a', 'a', 'a', '2020-10-01', '', ''),
(38, 'b', 'b', 'b', '2020-01-11', 'http://localhostrecursos/images.jpg', ''),
(39, 'h', 'h', 'h', '2020-10-01', 'http://localhostrecursos/charizard.jpg', ''),
(40, 'i', 'i', 'i', '2020-01-20', 'http://localhost/recursos/planeta.jpg', ''),
(41, 'p', 'p', 'p', '2020-10-01', '', ''),
(42, 'k', 'k', 'k', '2020-10-01', '', ''),
(43, 'k', 'k', 'k', '2020-10-01', '', ''),
(44, 'l', 'l', 'l', '2020-10-01', 'http://localhost/conexion/recursos/wp2487716.jpg', '');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `noticias`
--
ALTER TABLE `noticias`
  ADD PRIMARY KEY (`id_noti`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `noticias`
--
ALTER TABLE `noticias`
  MODIFY `id_noti` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
